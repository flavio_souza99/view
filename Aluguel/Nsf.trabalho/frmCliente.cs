﻿using Nsf.trabalho.Cadastar_Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Pedido_do_Cliente
{
    public partial class frmCliente : Form
    {
        public frmCliente()
        {
            InitializeComponent();
        }

        private void frmCliente_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar ir = new frmPedidoCadastrar();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.Cpf = txtCpf.Text;
                dto.Cnh = txtCnh.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Rua = txtRua.Text.Trim();
                dto.Bairro = txtBairro.Text.Trim();
                dto.Numero = txtNumero.Text.Trim();
                dto.Cep = txtCep.Text;


                if (dto.Nome == string.Empty || dto.Nome == "Digite seu nome")
                {
                    MessageBox.Show("Digite o Nome");

                    return;
                }

                if (dto.Cpf == string.Empty || dto.Cpf == "Digitar cpf" || txtCpf.Text.Length <= 11)
                {
                    MessageBox.Show("Digite o Cpf");

                    return;
                }

                if (dto.Cnh == string.Empty || dto.Cnh == "Digitar cnh" || txtCnh.Text.Length <= 10)
                {
                    MessageBox.Show("Digite a Cnh");

                    return;
                }

                if (dto.Telefone == string.Empty || dto.Telefone == "Digitar telefone" || txtTelefone.Text.Length <= 14)
                {
                    MessageBox.Show("Digite o Telefone");

                    return;
                }

                if (dto.Rua == string.Empty || dto.Rua == "Rua")
                {
                    MessageBox.Show("Digite a  Rua");

                    return;
                }

                if (dto.Bairro == string.Empty || dto.Bairro == "Bairro")
                {
                    MessageBox.Show("Digite o Bairro");

                    return;
                }

                if (dto.Numero == string.Empty || dto.Numero == "Nº")
                {
                    MessageBox.Show("Digite o Numero");

                    return;
                }

                if (dto.Cep == string.Empty || dto.Cep == "Digitar cep" || txtCep.Text.Length <= 7)
                {
                    MessageBox.Show("Digite o Cep");

                    return;
                }

                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(dto);

                MessageBox.Show("Cliente salvo com sucesso.", "Aluguel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Dados invalidos", "Aluguel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtTelefon_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtCpf_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtCnh_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true) || (char.IsSeparator(e.KeyChar) == true))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }

        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtNome_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "Digite seu nome")
            {
                txtNome.Text = string.Empty;
            }
        }

        private void txtRua_Click(object sender, EventArgs e)
        {
            if (txtRua.Text == "Rua")
            {
                txtRua.Text = string.Empty;
            }
        }

        private void txtNumero_Click(object sender, EventArgs e)
        {
            if (txtNumero.Text == "Nº")
            {
                txtNumero.Text = string.Empty;
            }
        }

        private void txtBairro_Click(object sender, EventArgs e)
        {
            if (txtBairro.Text == "Bairro")
            {
                txtBairro.Text = string.Empty;
            }
        }

        private void txtRua_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true) || (char.IsSeparator(e.KeyChar) == true))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }

        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true) || (char.IsSeparator(e.KeyChar) == true))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }
    }
}