﻿using Nsf.trabalho.Cadastrar_Carro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Colsultar__Carro
{
    public partial class frmCarros : Form
    {
        public frmCarros()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar ir = new frmPedidoCadastrar();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarroBusiness business = new CarroBusiness();
            List<CarroDTO> lista = business.Consultar(txtCarro.Text);

            dgvCarros.AutoGenerateColumns = false;
            dgvCarros.DataSource = lista;
        }

        private void dgvCarros_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvCarros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
