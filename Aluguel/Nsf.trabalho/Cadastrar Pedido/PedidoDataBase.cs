﻿using MySql.Data.MySqlClient;
using Nsf.trabalho.BancoDeDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastrar_Pedido
{
    class PedidoDataBase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido (id_cliente, nm_cartao, id_produto, nm_dia) VALUES (@id_cliente, @nm_cartao, @id_produto, @nm_dia)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ClienteId));
            parms.Add(new MySqlParameter("nm_cartao", dto.Cartao));
            parms.Add(new MySqlParameter("id_produto", dto.CarroId));
            parms.Add(new MySqlParameter("nm_dia", dto.Dia));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(PedidoDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_cliente = @nm_cliente,
                                     nm_cartao = @nm_cartao,
                                     nm_carro = @nm_carro,
                                     nm_dia = @nm_dia
                               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", dto.Id));
            parms.Add(new MySqlParameter("nm_cliente", dto.ClienteId));
            parms.Add(new MySqlParameter("nm_cartao", dto.Cartao));
            parms.Add(new MySqlParameter("nm_carro", dto.CarroId));
            parms.Add(new MySqlParameter("nm_dia", dto.Dia));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedido WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ConsultarPedidoView> Consultar(string pedido)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", pedido + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ConsultarPedidoView> lista = new List<ConsultarPedidoView>();
            while (reader.Read())
            {
                ConsultarPedidoView dto = new ConsultarPedidoView();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.Carro = reader.GetString("nm_carro");
                dto.Cartao = reader.GetString("nm_cartao");
                dto.Dia = reader.GetString("nm_dia");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<PedidoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> lista = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                parms.Add(new MySqlParameter("id_pedido", dto.Id));
                parms.Add(new MySqlParameter("nm_cliente", dto.ClienteId));
                parms.Add(new MySqlParameter("nm_cartao", dto.Cartao));
                parms.Add(new MySqlParameter("nm_carro", dto.CarroId));
                parms.Add(new MySqlParameter("nm_dia", dto.Dia));

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
