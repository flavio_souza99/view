﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastrar_Pedido
{
    class PedidoBusiness
    {

        public int Salvar(PedidoDTO dto)
        {
            PedidoDataBase db = new PedidoDataBase();
            return db.Salvar(dto);
        }

        public void Alterar(PedidoDTO dto)
        {
            PedidoDataBase db = new PedidoDataBase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            PedidoDataBase db = new PedidoDataBase();
            db.Remover(id);
        }

        public List<ConsultarPedidoView> Consultar(string produto)
        {
            PedidoDataBase db = new PedidoDataBase();
            return db.Consultar(produto);
        }

        public List<PedidoDTO> Listar()
        {
            PedidoDataBase db = new PedidoDataBase();
            return db.Listar();
        }
    }
}
