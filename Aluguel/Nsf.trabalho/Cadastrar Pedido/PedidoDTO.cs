﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastrar_Pedido
{
    class PedidoDTO
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public string Cartao { get; set; }
        public int CarroId { get; set; }
        public string Dia { get; set; }
    }
}
