﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastrar_Pedido
{
    class ConsultarPedidoView
    {
        public int Id { get; set; }
        public string Cliente { get; set; }
        public string Cartao { get; set; }
        public string Carro { get; set; }
        public string Dia { get; set; }
    }
}
