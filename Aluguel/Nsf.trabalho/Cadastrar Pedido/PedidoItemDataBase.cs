﻿using MySql.Data.MySqlClient;
using Nsf.trabalho.BancoDeDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastrar_Pedido
{
    class PedidoItemDataBase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_produto, id_pedido) VALUES (@id_produto, @id_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));
            parms.Add(new MySqlParameter("id_pedido", dto.IdPedido));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
