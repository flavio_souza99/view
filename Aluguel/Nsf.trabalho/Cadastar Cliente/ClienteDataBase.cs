﻿using MySql.Data.MySqlClient;
using Nsf.trabalho.BancoDeDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastar_Cliente
{
    class ClienteDataBase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_cliente, ds_cpf, ds_cnh, nu_telefone, nm_rua, nm_bairro, nu_numero, ds_cep )
                                              VALUES (@nm_cliente, @ds_cpf, @ds_cnh, @nu_telefone, @nm_rua, @nm_bairro, @nu_numero, @ds_cep)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("ds_cnh", dto.Cnh));
            parms.Add(new MySqlParameter("nu_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nm_rua", dto.Rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("nu_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente 
                                 SET nm_cliente = @nm_cliente,
                                     ds_cpf  = @ds_cpf,
                                     ds_cnh = @ds_cnh,
                                     nu_telefone = @nu_telefone,
                                     nm_rua = @nm_rua,
                                     nm_bairro = @nm_bairro,
                                     nu_numero = @nu_numero,
                                     ds_cep =  @ds_cep
                               WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("ds_cnh", dto.Cnh));
            parms.Add(new MySqlParameter("nu_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nm_rua", dto.Rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("nu_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Cnh = reader.GetString("ds_cnh");
                dto.Telefone = reader.GetString("nu_telefone");
                dto.Rua = reader.GetString("nm_rua");
                dto.Bairro = reader.GetString("nm_bairro");
                dto.Numero = reader.GetString("nu_numero");
                dto.Cep = reader.GetString("ds_cep");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Cnh = reader.GetString("ds_cnh");
                dto.Telefone = reader.GetString("nu_telefone");
                dto.Rua = reader.GetString("nm_rua");
                dto.Bairro = reader.GetString("nm_bairro");
                dto.Numero = reader.GetString("nu_numero");
                dto.Cep = reader.GetString("ds_cep");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
