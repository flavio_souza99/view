﻿using Nsf.trabalho.Cadastar_Cliente;
using Nsf.trabalho.Cadastrar_Carro;
using Nsf.trabalho.Cadastrar_Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmPedido : Form
    {
        BindingList<CarroDTO> produtosCarrinho = new BindingList<CarroDTO>();
        BindingList<ClienteDTO> produtosCarrinhos = new BindingList<ClienteDTO>();

        public frmPedido()
        {
            InitializeComponent();
            CarregarCombos();
            CarregarCombo();
          
        }

        void CarregarCombos()
        {
            CarroBusiness business = new CarroBusiness();
            List<CarroDTO> lista = business.Listar();

            cboCarro.ValueMember = nameof(CarroDTO.Id);
            cboCarro.DisplayMember = nameof(CarroDTO.Carro);
            cboCarro.DataSource = lista;
        }

        void CarregarCombo()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Listar();

            cboCliente.ValueMember = nameof(ClienteDTO.Id);
            cboCliente.DisplayMember = nameof(ClienteDTO.Nome);
            cboCliente.DataSource = lista;
        }  

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar ir = new frmPedidoCadastrar();
            this.Hide();
            ir.ShowDialog();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarroDTO dtos = cboCarro.SelectedItem as CarroDTO;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClienteDTO dto = cboCliente.SelectedItem as ClienteDTO;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = cboCliente.SelectedItem as ClienteDTO;
                CarroDTO carro = cboCarro.SelectedItem as CarroDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.ClienteId = cliente.Id;
                dto.Cartao = txtCartao.Text.Trim();
                dto.CarroId = carro.Id;
                dto.Dia = txtQuantidade.Text.Trim();



                if (dto.Cartao == string.Empty)
                {
                    MessageBox.Show("Digitar a forma de pagamento");

                    frmPedido ir = new frmPedido();
                    this.Hide();
                    ir.ShowDialog();
                }


                if (dto.Dia == string.Empty || dto.Dia == "Digitar o Dia" || txtQuantidade.Text.Length <= 0)
                {
                    MessageBox.Show("Digitar o Dia");

                    frmPedido ir = new frmPedido();
                    this.Hide();
                    ir.ShowDialog();
                }

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Pedido salvo com sucesso.", "Aluguel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show ("É preciso cadastrar o carro e o cliente antes", "Aluguel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                frmPedido ir = new frmPedido();
                this.Hide();
                ir.ShowDialog();
            }
        }

        

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmPedido_Load(object sender, EventArgs e)
        {

        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtQuantidade_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
