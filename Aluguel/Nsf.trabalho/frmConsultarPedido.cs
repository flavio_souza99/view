﻿using Nsf.trabalho.Cadastrar_Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmConsultarPedido : Form
    {
        public frmConsultarPedido()
        {
            InitializeComponent();
        }

        private void frmConsultarPedido_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar ir = new frmPedidoCadastrar();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PedidoBusiness business = new PedidoBusiness();
            List<ConsultarPedidoView> lista = business.Consultar(txtCliente.Text.Trim());

            dgvPedido.AutoGenerateColumns = false;
            dgvPedido.DataSource = lista;
        }

        private void dgvPedido_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                ConsultarPedidoView pedido = dgvPedido.Rows[e.RowIndex].DataBoundItem as ConsultarPedidoView;

                DialogResult r = MessageBox.Show($"Deseja realmente excluir o pedido {pedido.Id}?", "Aluguel",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    PedidoBusiness business = new PedidoBusiness();
                    business.Remover(pedido.Id);

                    button4_Click(null, null);
                }
            }
        }
    }
}
