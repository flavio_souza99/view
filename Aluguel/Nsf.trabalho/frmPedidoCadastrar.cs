﻿using Nsf.trabalho.Telas.Cadastro_de_Carro;
using Nsf.trabalho.Telas.Colsultar__Carro;
using Nsf.trabalho.Telas.Pedido_do_Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmPedidoCadastrar : Form
    {
        public frmPedidoCadastrar()
        {
            InitializeComponent();
        }



        private void frmPedidoCadastrar_Load(object sender, EventArgs e)
        {
          
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmlogar retroceder = new frmlogar();
            this.Hide();
            retroceder.ShowDialog();
            
            
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrarCarros ir = new frmCadastrarCarros();
            this.Hide();
            ir.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmCarros ir = new frmCarros();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCliente ir = new frmCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmConsultarCliente ir = new frmConsultarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmPedido ir = new frmPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
