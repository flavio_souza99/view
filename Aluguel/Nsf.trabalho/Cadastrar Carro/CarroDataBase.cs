﻿using MySql.Data.MySqlClient;
using Nsf.trabalho.BancoDeDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastrar_Carro
{
    class CarroDataBase
    {
        public int Salvar(CarroDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_carro, vl_preco) VALUES (@nm_carro, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_carro", dto.Carro));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(CarroDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_carro = @nm_carro,
                                     vl_preco   = @vl_preco
                               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_carro", dto.Carro));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public List<CarroDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_carro like @nm_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_carro", produto + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarroDTO> lista = new List<CarroDTO>();
            while (reader.Read())
            {
                CarroDTO dto = new CarroDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Carro = reader.GetString("nm_carro");
                dto.Preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<CarroDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarroDTO> lista = new List<CarroDTO>();
            while (reader.Read())
            {
                CarroDTO dto = new CarroDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Carro = reader.GetString("nm_carro");
                dto.Preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
