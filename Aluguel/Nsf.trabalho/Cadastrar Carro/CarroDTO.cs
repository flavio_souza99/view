﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cadastrar_Carro
{
    class CarroDTO
    {
        public int Id { get; set; }
        public string Carro { get; set; }
        public decimal Preco { get; set; }
    }
}
