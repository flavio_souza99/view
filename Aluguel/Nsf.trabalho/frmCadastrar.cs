﻿using Nsf.trabalho.BD_Login_class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar telapedido = new frmPedidoCadastrar();

            string nome = txtnome.Text.Trim();
            string senha = txtsenha.Text.Trim();

            if (nome == string.Empty || nome == "Crie um E-mail")
            {
                MessageBox.Show("O campo E-mail é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                frmCadastrar ir = new frmCadastrar();
                this.Hide();
                ir.ShowDialog();

            }

            if (senha == string.Empty || senha == "qualquer")
            {
                MessageBox.Show("O campo senha é obrigatorio", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                frmCadastrar ir = new frmCadastrar();
                this.Hide();
                ir.ShowDialog();
            }

            loginDTO dados = new loginDTO();
            dados.nome = nome;
            dados.senha = senha;
            LoginBusiness salvardados = new LoginBusiness();
            salvardados.Salvar(dados);

            MessageBox.Show("Cadastrado");

            
            frmlogar voltar = new frmlogar();
            this.Hide();
            voltar.ShowDialog();
            

        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtsenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmlogar ir = new frmlogar();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true) || (char.IsSymbol(e.KeyChar) == true)
                 || (char.IsPunctuation(e.KeyChar) == true))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtnome_Click(object sender, EventArgs e)
        {
            if (txtnome.Text == "Crie um E-mail")
            {
                txtnome.Text = string.Empty;
            }
        }

        private void txtsenha_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == "qualquer")
            {
                txtsenha.Text = string.Empty;
            }
        }
    }
}
