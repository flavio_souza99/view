﻿using MySql.Data.MySqlClient;
using Nsf.trabalho.BancoDeDados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.BD_Login_class
{
    public class LoginDataBase
    {
        public bool Logar(string nome, string senha)
        {
            string script = 
                              @"SELECT *  
                              FROM logar 
                              where nome = @nome and
                                           senha = @senha";

            List<MySqlParameter> parametros = new List<MySqlParameter>();
            parametros.Add(new MySqlParameter("nome", nome));
            parametros.Add(new MySqlParameter("senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parametros);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public int Salvar(loginDTO dados)
        {
            string script =
             @"INSERT INTO logar (nome, senha)
                          VALUES (@nome, @senha)";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("nome", dados.nome));
            parms.Add(new MySqlParameter("senha", dados.senha));


            DataBase db = new DataBase();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
    }
}
