﻿using Nsf.trabalho.Cadastrar_Carro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Cadastro_de_Carro
{
    public partial class frmCadastrarCarros : Form
    {
        public frmCadastrarCarros()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                CarroDTO dto = new CarroDTO();
                dto.Carro = txtCarro.Text.Trim();
                dto.Preco = Convert.ToDecimal(txtPreco.Text);

                if (dto.Carro == string.Empty || dto.Carro == "Digitar o Carro")
                {
                    MessageBox.Show("Digite o Carro");
                    return;
                }

                else if (dto.Preco == 0)
                {
                    MessageBox.Show("Digite o Preço");
                    return;
                }

                CarroBusiness business = new CarroBusiness();
                business.Salvar(dto);

                MessageBox.Show("Carro salvo com sucesso.", "Aluguel", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Digite o preço", "Aluguel", MessageBoxButtons.OK, MessageBoxIcon.Information);
                frmCadastrarCarros ir = new frmCadastrarCarros();
                this.Hide();
                ir.ShowDialog();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmPedidoCadastrar ir = new frmPedidoCadastrar();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || (char.IsControl(e.KeyChar) == true) || (char.IsPunctuation(e.KeyChar) == true))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtPreco_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
